package img;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

import javax.imageio.ImageIO;

public class ImgRecovery {
	private static int[] xzuobiao = {157,145,265,277,181,169,241,253,109,97,289,301,85,73,25,37,13,1,121,133,61,49,217,229,205,193
	                              ,145,157,277,265,169,181,253,241,97,109,301,289,73,85,37,25,1,13,133,121,49,61,229,217,193,205};
	private static int[] yzuobiao = {58,0};
	
	public static BufferedImage recovery(InputStream yinying){
		BufferedImage yinyingimg;
		BufferedImage img;
		try {
			yinyingimg = ImageIO.read(yinying);
			img = new BufferedImage(260,yinyingimg.getHeight(), BufferedImage.TYPE_INT_RGB);
			Graphics2D  gp = img.createGraphics();
			int x1 = 0;
			for(int i=0;i<xzuobiao.length/2;i++){
				gp.drawImage(yinyingimg, x1, yzuobiao[1], 10+x1, 58+ yzuobiao[1], xzuobiao[i], yzuobiao[0], 10+xzuobiao[i], 58+yzuobiao[0], null);
				x1+=10;
			}
			x1 = 0;
			for(int i=xzuobiao.length/2;i<xzuobiao.length;i++){
				gp.drawImage(yinyingimg, x1, yzuobiao[0], 10+x1, 58+yzuobiao[0], xzuobiao[i], yzuobiao[1], 10+xzuobiao[i], 58+yzuobiao[1], null);
				x1+=10;
			}
			gp.dispose();
			File file = new File("test" + new Random().nextInt()  + ".jpg");
//			if(!file.exists()){
//				file.mkdir();
//			}
			ImageIO.write(img, "jpg",file );
			return img;
		} catch (IOException e) {
			e.printStackTrace();
			return new BufferedImage(260,116, BufferedImage.TYPE_INT_RGB);
		}
	}
	public static byte[] recovery(File yinying){
		BufferedImage yinyingimg;
		BufferedImage img;
		try {
			yinyingimg = ImageIO.read(yinying);
			img = new BufferedImage(260,yinyingimg.getHeight(), BufferedImage.TYPE_INT_RGB);
			Graphics2D  gp = img.createGraphics();
			int x1 = 0;
			for(int i=0;i<xzuobiao.length/2;i++){
				gp.drawImage(yinyingimg, x1, yzuobiao[1], 10+x1, 58+ yzuobiao[1], xzuobiao[i], yzuobiao[0], 10+xzuobiao[i], 58+yzuobiao[0], null);
				x1+=10;
			}
			x1 = 0;
			for(int i=xzuobiao.length/2;i<xzuobiao.length;i++){
				gp.drawImage(yinyingimg, x1, yzuobiao[0], 10+x1, 58+yzuobiao[0], xzuobiao[i], yzuobiao[1], 10+xzuobiao[i], 58+yzuobiao[1], null);
				x1+=10;
			}
			gp.dispose();
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
//			File file = new File("test" + new Random().nextInt()  + ".jpg");
//			if(!file.exists()){
//				file.mkdir();
//			}
			ImageIO.write(img, "jpg",baos );
			return baos.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
			return new byte[1024];
		}
	}
	public static void main(String[] args){
		File yuantu = new File("testimg/yuantu.jpg");
		File yinying = new File("testimg/yinying.jpg");
		byte[] yuantuimg = ImgRecovery.recovery(yuantu);
		byte[] yinyingimg = ImgRecovery.recovery(yinying);
		byte[] result = new byte[yuantuimg.length];
		for(int i = 0;i < yuantuimg.length;i++){
			result[i] = (byte) (yuantuimg[i] - yinyingimg[i]);
			if(result[i]>100){
				System.out.println(i%260);
				break;
			}
		}
	}
}
