package http;

import img.ImgRecovery;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;

import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;

import com.google.gson.Gson;

/**
 * Created by yuanshichao on 15/11/7.
 */
public class GeetestGet {

    private static final String URL = "https://api.geetest.com/get.php";
    private static final String USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36";
    private static final String COOKIE = "GeeTestUser=a7ebe58ffc88184353dac46bde670001; Hm_lvt_3da418e020135b5d756a050ce5fd29d9=1446862546; Hm_lpvt_3da418e020135b5d756a050ce5fd29d9=1446863325";
    private static final String IMAGE_URL_PREFIX = "https://static.geetest.com/";

    private static final Gson GSON = new Gson();

    private Geetest geetest;
    private byte[] bg;
    private byte[] fullbg;

    public GeetestGet(String gt) {
        try {
            this.geetest = getResource(gt);
        } catch (Throwable t) {
            System.out.println("获取验证码失败");
            throw new RuntimeException("获取验证码失败");
        }
    }

    private Geetest getResource(String gt) {

        HttpUriRequest req =
                RequestBuilder
                        .get(URL)
                        .addHeader("User-Agent", USER_AGENT)
                        .addHeader("Cookie", COOKIE)
                        .addParameter("gt", gt)
                        .build();

        String respStr = HttpClientUtil.send(req);
        Pattern p = Pattern.compile("new Geetest\\((.*),true\\)\\.appendTo");
        Matcher m = p.matcher(respStr);
        m.find();
        String jsonStr = m.group(1);
        Geetest geetest = GSON.fromJson(jsonStr, Geetest.class);

        System.out.println(geetest);

        this.bg = HttpClientUtil.getBytes(RequestBuilder.get(IMAGE_URL_PREFIX+geetest.getBg()).build());
        this.fullbg = HttpClientUtil.getBytes(RequestBuilder.get(IMAGE_URL_PREFIX+geetest.getFullbg()).build());

        return geetest;

    }

    public byte[] getBg() {
        return bg;
    }

    public byte[] getFullbg() {
        return fullbg;
    }

    public Geetest getGeetest() {
        return geetest;
    }

    public static void main(String[] args) {
        GeetestGet get = new GeetestGet("cddd9a3ffbb579d1e2023e2c76c817b9");


        BufferedImage yuantuimg = ImgRecovery.recovery(new ByteArrayInputStream(get.getFullbg()));
        BufferedImage yinyingimg = ImgRecovery.recovery(new ByteArrayInputStream(get.getBg()));

        int[] avgArray = new int[260];

        //42

        int yinyingRGB = 0;
        int yuantuRGB = 0;
        for(int x=0;x<260;x++){

            int sum = 0;

            for(int y=0;y<yinyingimg.getHeight();y++){

                yinyingRGB = yinyingimg.getRGB(x, y);

                int yinyingGray = (yinyingimg.getColorModel().getBlue(yinyingRGB)
                        + yinyingimg.getColorModel().getRed(yinyingRGB)
                        + yinyingimg.getColorModel().getGreen(yinyingRGB))/3;

                yuantuRGB = yuantuimg.getRGB(x, y);

                int yuantuGray = (yuantuimg.getColorModel().getBlue(yuantuRGB)
                        + yuantuimg.getColorModel().getRed(yuantuRGB)
                        + yuantuimg.getColorModel().getGreen(yuantuRGB))/3;


                int rgb = yuantuGray - yinyingGray;

                sum += rgb;

                //-16;

                yuantuimg.setRGB(x, y, (rgb << 16) + (rgb << 8) + rgb);
            }
            int a = (sum/yinyingimg.getHeight()&0xff);
            avgArray[x] = a>245?255:(a<10?0:sum/yinyingimg.getHeight());
        }
        for(int x=0;x<avgArray.length;x++){
        	for(int y=0;y<yinyingimg.getHeight();y++){
        		yuantuimg.setRGB(x, y, avgArray[x]);
        	}
        	if(avgArray[x]!=0){
        		System.out.println(x);
        	}
        }
        System.out.println(Arrays.toString(avgArray));
        try {
            File file = new File("./test1.jpg");
            ImageIO.write(yuantuimg, "jpg",file );
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
