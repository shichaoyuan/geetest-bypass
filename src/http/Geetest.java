package http;

/**
 * Created by yuanshichao on 15/11/7.
 */
public class Geetest {

    private String id;
    private String gt;
    private String challenge;
    private String bg;
    private String fullbg;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGt() {
        return gt;
    }

    public void setGt(String gt) {
        this.gt = gt;
    }

    public String getChallenge() {
        return challenge;
    }

    public void setChallenge(String challenge) {
        this.challenge = challenge;
    }

    public String getBg() {
        return bg;
    }

    public void setBg(String bg) {
        this.bg = bg;
    }

    public String getFullbg() {
        return fullbg;
    }

    public void setFullbg(String fullbg) {
        this.fullbg = fullbg;
    }

    @Override
    public String toString() {
        return "Geetest{" +
                "id='" + id + '\'' +
                ", gt='" + gt + '\'' +
                ", challenge='" + challenge + '\'' +
                ", bg='" + bg + '\'' +
                ", fullbg='" + fullbg + '\'' +
                '}';
    }
}
